from django.db import models
from django.contrib.auth.models import User



class Article(models.Model):
	title 		=  models.CharField(max_length=200)
	slug		=  models.SlugField(max_length=200)
	author	 	=  models.ForeignKey('Author')
	pub_date	=  models.DateField()
	body		=  models.TextField()





class Author(models.Model):
	#user        = models.OneToOneField(User)
	first_name  = models.CharField(max_length=100)
	last_name	= models.CharField(max_length=100)
	nick_name 	= models.CharField(max_length=100)
	slug		= models.SlugField(max_length=200)
	birthday	= models.DateField()
	Job			= models.TextField(blank=True)
	bio		 	= models.TextField(blank=True)

	def __unicode__(self):
		return self.nick_name


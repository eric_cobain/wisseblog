from django.contrib import admin
from blog.models import Article, Author

class ArticleAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug':('title',)}
	list_display		= ('title', 'pub_date', 'author')


class AuthorAdmin(admin.ModelAdmin):
	prepopulated_fields	= {'slug':('nick_name',)}
	list_display		= ('first_name', 'last_name', 'nick_name')

admin.site.register(Article, ArticleAdmin)	
admin.site.register(Author, AuthorAdmin)

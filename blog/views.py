from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from blog.models import Article, Author
from blog.forms import RegistrationForm, LoginForm
from django.contrib.auth import authenticate, login, logout

def list_articles(request):
	articles = Article.objects.all().order_by('-pub_date')
	context = {'articles':articles, 'tweets':get_tweets()}
	return render_to_response('articles.html', context, context_instance=RequestContext(request))

def specific_article(request, article_slug):
	article = Article.objects.get(slug=article_slug)
	context = {'article':article, 'tweets':get_tweets()}
	return render_to_response('article.html', context, context_instance=RequestContext(request))

def portfolio(request):
	return render_to_response('portfolio.html',context_instance=RequestContext(request))



def AuthorRegistration(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/dash')
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user 			= User.objects.create_user(username=form.cleaned_data['username'], email = form.cleaned_data['email'], password = form.cleaned_data['password'])
            user.save()
            author 			= user.get_profile()
            author.name 	= form.cleaned_data['name']
            #warper.birthday = form.cleaned_data['birthday']
            author.save()
            username 		= form.cleaned_data['username']
            password	 	= form.cleaned_data['password']
            author			= authenticate (username=username, password=password)
            login(request, author)
            return HttpResponseRedirect('/dash/')
        else:
            return render_to_response('register.html', {'form':form}, context_instance=RequestContext(request))                            
                
    else: 
        ''' user is not submitting the form, show them a blank registration form '''
        form  = RegistrationForm()
        context = {'form':form}
        return render_to_response('register.html', context, context_instance=RequestContext(request))
        
@login_required
def Profile(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    warper = request.user.get_profile
    context = {'author':author}
    return render_to_response('profile.html', context, context_instance=RequestContext(request))
    
                 
def LoginRequest(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/dash/')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
           
            if form.cleaned_data['username']:
                username 	= form.cleaned_data['username']
                password 	= form.cleaned_data['password']
                author		= authenticate (username=username, password=password)

            elif form.cleaned_data['email']:
                email 		= form.cleaned_data['email']
                password 	= form.cleaned_data['password']
                author		= authenticate (email=email, password=password)

            
            
            if author is not None:
                login(request, author)
                return HttpResponseRedirect('/dash/')
            else:
                return render_to_response('login.html', {'form':form}, context_instance=RequestContext(request))
                        
            
    else:
        ''' user is not submmitting  the form, show the login form'''
        form = LoginForm()
        context = {'form':form}
        return render_to_response('login.html', context, context_instance=RequestContext(request))
        
def LogoutRequest(request):
    logout(request)
    return HttpResponseRedirect('/')         








'''internal function '''

def get_tweets():
	tweets = []
	try:
		api_key = 'dit74kZVGTVzz7w'
		api_secret ='jkQnjTWC6vP7nNqDp0eJysUlcLeErFU'
		access_key = '605222489-d9CgmXtsrAOCB4uV5XRvPzXaHmfZ'
		access_secret = 'NoKfckHs3NZBzckgPpGyq' 
	
		
		import twitter
		api = twitter.Api(consumer_key=api_key, consumer_secret=api_secret, access_token_key=access_key, access_token_secret=access_secret, cache=None)
		latest = api.GetUserTimeline('ericobain')
		for tweet in latest: 
			status = tweet.text
			tweet_date = tweet.relative_created_at
			tweets.append({'status':status, 'date':tweet_date})
	except:
		tweets.append({'status':'Stay tuned for more of this kinda stuff', 'date':'10 minutes ago'})
	return {'tweets': tweets }			
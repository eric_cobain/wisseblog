from django.conf.urls.defaults import *
from django.contrib import admin
import dbindexer

handler500 = 'djangotoolbox.errorviews.server_error'

# django admin
admin.autodiscover()

# search for dbindexes.py in all INSTALLED_APPS and load them
dbindexer.autodiscover()

urlpatterns = patterns('',
    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
    ('^admin/', include(admin.site.urls)),
    ('^$', 'blog.views.list_articles'),
    ('^portfolio/$', 'blog.views.portfolio'), 

    #Always goes last... a bit wild.
    ('^(?P<article_slug>.*)/$', 'blog.views.specific_article'),
)
